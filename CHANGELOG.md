# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.3.8

- patch: Internal maintenance: Add gitignore secrets.

## 0.3.7

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.

## 0.3.6

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.3.5

- patch: Add warning message about new version of the pipe available.

## 0.3.4

- patch: Updated pipes toolkit version to fix coloring of log info messages.

## 0.3.3

- patch: Documentation updates

## 0.3.2

- patch: Internal maintenance: update pipes toolkit version.

## 0.3.1

- patch: Fix fail status processing

## 0.3.0

- minor: BITBUCKET_REPO_OWNER is deprecated in favor of BITBUCKET_WORKSPACE

## 0.2.4

- patch: Improved the outputs of the pipe by streaming the process in realtime

## 0.2.3

- patch: Fix crashes the pipe in debug mode

## 0.2.2

- patch: Minor documentation updates

## 0.2.1

- patch: Updated contributing guidelines

## 0.2.0

- minor: Changed the docker image in build

## 0.1.0

- minor: Initial release
- patch: Fixed CI scripts

